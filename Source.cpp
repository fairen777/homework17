#include <iostream>
#include <cmath>
#include <Windows.h>

class DateToday
{
private:
	int day_, month_, year_;
public:
	DateToday() : day_(0), month_(0), year_(0)
	{}
	DateToday(int day, int month, int year) : day_(day), month_(month), year_(year)
	{
	}
	void SetDay(int day)
	{
		day_ = day;
	}
	void SetMonth(int month)
	{
		month_ = month;
	}
	void SetYear(int year)
	{
		year_ = year;
	}
	void ShowDate()
	{
		std::cout << day_ << '/' << month_ << '/' << year_ << std::endl;
	}
};

class Vector
{
private:
	double x_, y_, z_;
public:
	Vector() : x_(0),y_(0),z_(0)
	{}
	Vector(double x, double y, double z) : x_(x), y_(y), z_(z)
	{
	}
	void ShowCoordinates()
	{
		std::cout << x_ << ' ' << y_ << ' ' << z_ << std::endl;
	}
	double getVectorLenght()
	{
		return sqrt((x_*x_)+(y_*y_)+(z_*z_));
	}
};

int main()
{
	DateToday today(6,9,2021);
	today.SetDay(7);
	today.ShowDate();

	Vector vec(10,10,10);
	vec.ShowCoordinates();
	std::cout << vec.getVectorLenght() << std::endl;
}